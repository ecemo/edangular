import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from "@angular/core";
import { Curso } from "../curso";
import { Router } from "@angular/router";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { CoursesService } from "../courses.service";
import { tap, catchError } from 'rxjs/operators';
import { of, EMPTY } from 'rxjs';

@Component({
  selector: "app-courses",
  templateUrl: "./courses.component.html",
  styleUrls: ["./courses.component.scss"],
})
export class CoursesComponent implements OnInit, AfterViewInit {
  private _textoFiltro: string = "";

  cursos: Curso[];
  mensajeError:string;
  set textoFiltro(t: string) {
    console.log("textoFiltro", t);
    this._textoFiltro = t;
    //filtrado de cursos
   /*  this.cursos = t ? this.filtrarCursos(t) : this.coursesServices.getCourses(); */
  }

  get textoFiltro() {
    return this._textoFiltro;
  }

  //getelemetbyid
  @ViewChild("filtro", { static: false })
  filtro: ElementRef;


  title: string = "bienvenido la lista de cursos";

  widthImg: string = "100px";

  constructor(private router: Router, private coursesServices: CoursesService) {

  }

  ngOnInit(): void {

       // this.cursos = this.coursesServices.getCursos().subscribe();

       this.coursesServices.getCursos().pipe(

        //debugging to print the data
         tap(cursos=>console.log('Cursos',cursos)),
         catchError(error=>{
           this.mensajeError=error;
           //catch and replace
           return EMPTY;
         }
          

          )
       )
       .subscribe((data:Curso[])=> this.cursos=data );
  }

  ngAfterViewInit(): void {
    //aqui estamos accediendo al valor del input
   /*  this.filtro.nativeElement.value = "Angular"; */
  }

  //estos metodos se definieron en la directiva de el hijo
  onEditCurso(curso: Curso): void {
    this.router.navigate([`courses/${curso.id}`]);
  }
  onDeleteCurso(curso: Curso): void {
    this.cursos = this.cursos.filter((c: Curso) => {
      return c.id !== curso.id;
    });
  }
  filtrarCursos(t: string):Curso[] {
   return this.cursos.filter((curso: Curso) => {
      return curso.name.toLowerCase().includes(t.toLowerCase());
    });
  }
}
