import { Component, OnInit, ViewChild } from "@angular/core";
import { Curso } from "../curso";
import { FormControl } from '@angular/forms';
@Component({
  selector: "app-course-add",
  templateUrl: "./course-add.component.html",
  styleUrls: ["./course-add.component.scss"],
})
export class CourseAddComponent implements OnInit {
  //el modelo del formulario
  model: Curso = {};

//get form from the DOM #formularioAdd 
  @ViewChild('formularioAdd',{static:true})form:FormControl;

  constructor() {}

  ngOnInit(): void {}

  onSubmit(){
    console.log('onSubmit',this.form);
    if (this.form.valid) {
      //enviar a el backend
      
      this.form.reset();
    }



  }
}
