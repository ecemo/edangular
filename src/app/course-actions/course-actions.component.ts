import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {Curso} from "../curso"  


@Component({
  selector: "app-course-actions",
  templateUrl: "./course-actions.component.html",
  styleUrls: ["./course-actions.component.scss"],
})
export class CourseActionsComponent implements OnInit {

  @Input() 
  curso:Curso;
//aqui van definidos los disparadores de los metotodos que estan el el padre
  @Output()
  edit:EventEmitter<Curso>= new EventEmitter<Curso>();
  @Output()
  delete:EventEmitter<Curso>= new EventEmitter<Curso>();
  constructor() {}

  ngOnInit(): void {}

  editarCurso(curso: Curso) {
    console.log(curso);
    //cuando se manda a llamar este metodo se puede usar los disparadores de los metodos
    //se manda al componente padre
    this.edit.emit(curso);
  }
  onMouseOver(event: any) {
    console.log(event);
  }
  doubleClick(event: any) {
    console.log(event);
  }
  remove(curso:Curso) {
    console.log(curso);
    this.delete.emit(curso);

  }
}
