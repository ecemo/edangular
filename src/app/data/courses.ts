
import { Curso } from "../curso";
export const COURSES:Curso[]=
    [
        {
          id:1,
          imageUrl: "assets/images/codigo.png",
          name: "Angular",
          description: "Angular desde cero",
          startDate: 10-10-2020,
          price: 455.78,
          rating: 4.9,
        },
        {
          id:2,
          imageUrl: "assets/images/programacion.png",
          name: "Typescript",
          description: "Typescript desde cero",
          startDate: 11-10-2020,
          price: 455.78,
          rating: 4.5,
        },
        {
          id:3,
          imageUrl: "assets/images/programacion.png",
          name: " Node",
          description: "Node desde cero",
          startDate: 12-10-2020,
          price: 455.78,
          rating: 3.9,
        },
      ];
