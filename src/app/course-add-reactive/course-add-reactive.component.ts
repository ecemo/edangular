import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ControlContainer, } from "@angular/forms";

@Component({
  selector: "app-course-add-reactive",
  templateUrl: "./course-add-reactive.component.html",
  styleUrls: ["./course-add-reactive.component.scss"],
})
export class CourseAddReactiveComponent implements OnInit {
  //esta variable sirve como referencia para el formulario reactivo.
  courseAddForm: FormGroup;
  priceAddControl:FormControl = new FormControl(null,[Validators.required,this.minPrice(10)]);

  constructor() {}

  ngOnInit(): void {
    //se inicializa el formulario y para cada campo del formulario se se tiene que crear un objeto

    this.courseAddForm = new FormGroup({
      name: new FormControl(null,Validators.required),
      description: new FormControl(null,[Validators.required,Validators.minLength(5)]),
     /*  price: new FormControl(null,[Validators.required,this.minPrice(10)]), */
      price: this.priceAddControl,
      url: new FormControl(null),
    });
   /*  this.courseAddForm.valueChanges.subscribe(value=>console.log(value)) */

   /* this.courseAddForm.statusChanges.subscribe(status=>console.log(status)); */
   
   this.priceAddControl.valueChanges.subscribe(changes=>console.log(changes));
   
  }

  onSubmit(): void {
    console.log("onSubmit", this.courseAddForm);
   
  }
  get price(){
    return this.courseAddForm.get('price');
  }

//VALIDADOR PERSONALIZADO
minPrice(minPrice:number): ValidatorFn{
  return(control:AbstractControl):{[key: string]:any  } | null =>{
    if(control.value !== undefined && control.value <=minPrice ){
      return {
         'minPrice':true
      }
    }
    else{
      return null;
    }
  }
}


}
