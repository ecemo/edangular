export interface Curso {
   id?:number;
    name?: string,
    description?: string,
    startDate?: number,
    price?: number,
    rating?: number,
    imageUrl?: string,
}
