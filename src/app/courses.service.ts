import { Injectable } from '@angular/core';
import { Curso } from './curso';
import { COURSES } from "./data/courses";
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class CoursesService {


  //injection
  constructor(private http:HttpClient) { }
/* 
  getCourses():Curso[]{
     
  
    return COURSES;
  } */
  getCursos():Observable<Curso[]>{

    return this.http.get<Curso[]>("assets/API/courses/courses.json").pipe(

      catchError(this.manejarError) )
  }


  manejarError(error:HttpErrorResponse){

    if(error.error instanceof ErrorEvent){
      console.log('error de cliente',error.error.message);
    }
    else{
           //Error del servidor
           console.log('Error status:',error.status);
           console.log('Error',error.error);
    }

    return throwError('Hubo un problema al obtener los tados');
  }
}
