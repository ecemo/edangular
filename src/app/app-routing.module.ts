import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseEditComponent } from './course-edit/course-edit.component';
import { CourseAddComponent } from './course-add/course-add.component';
import { CourseAddReactiveComponent } from './course-add-reactive/course-add-reactive.component';

const routes: Routes = [
  {path:'',redirectTo:'courses',pathMatch:'full'},
  {path:'dashboard',component:DashboardComponent},
  {path:'navbar',component:NavbarComponent},
  {path:'sidebar',component:SidebarComponent},
  {path:'courses',component:CoursesComponent},
  {path:'courses/edit/:id',component:CourseEditComponent},
  {path:'courses/add',component:CourseAddComponent},
  {path:'courses/add/reactive',component:CourseAddReactiveComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
